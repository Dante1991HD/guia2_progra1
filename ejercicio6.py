#!/usr/bin/python
# -*- coding: utf-8 -*-

# se importa la libreria random

import random
from collections import Counter
# se declaran las variables

lista = []
palabras = ['Buen', 'Día', 'a', 'Papá', 'Di', 'perro', 'gato', 'python', 'tomar']

# la función "lista_palabras_aleatorias" crea una lista de manera aleatoria con las palabras contenidas en otra lista

def listas_palabras_aleatorias():
    n_listas = int(input("Ingrese la cantidad de listas a generar"))
    listas = []
    for j in range(n_listas):
        lista.append([])
        for i in range(random.randint(1,10)):
            lista[j].append(random.choice(palabras))
        listas += lista[j]
    lista_sin_duplicados = list(dict.fromkeys(listas))
    print("Listas creadas\n", lista, "\nLista sin duplicados\n", lista_sin_duplicados)
    print("Palabra más repetida:", Counter(listas).most_common()[0])
    print("Palabra menos repetida (un ejemplo):", Counter(listas).most_common()[-1])


# El llamando de esta función da inicio al programa
listas_palabras_aleatorias()
