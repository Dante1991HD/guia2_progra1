#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# Se importa la libreria random

import random


# Función que compara el entero k con las listas y la distribulle según la condición
def listas(entero_k, lista_numeros):
    menores = sorted([x for x in lista_numeros if x < entero_k])
    mayores = sorted([x for x in lista_numeros if x > entero_k])
    iguales = sorted([x for x in lista_numeros if x == entero_k])
    multiplos = [entero_k * x for x in range(1, 11)]
    print('Menores', menores, '\nmayores', mayores, '\niguales', iguales,
          '\nmultiplos', multiplos)


# Función en la que se pide un entero k al usuario y se genera una lista
def numeros():
    lista_numeros = [random.randint(0, 9) for x in range(25)]
    print(lista_numeros)
    entero_k = int(input("ingrese un numero entero "))
    listas(entero_k, lista_numeros)


# El llamado de esta función da inicio al programa
numeros()
