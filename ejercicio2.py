#!/usr/bin/env python3
# -*- coding:utf-8 -*-
# se declara la matriz
matriz = []


# Función que imprime la matriz creada
def impresora(matriz):
    for i in range(len(matriz)):
        for j in range(len(matriz[i])):
            print(matriz[i][j], end='')
        print()
    print()


# Función que en base a la altura dada crea una matriz que contiene la figura requerida
def figura(altura):
    matriz = [[
        '*' * (i + 1), ' ' * ((altura * 2) - (2 * i+2)), '*' * (i + 1)
    ] for i in range(altura)]
    impresora(matriz)


# Función que pide al usuario el tamaño de la figura a crear
def tamano_figura():
    tamano = int(input("Ingrese la altura de los triángulos "))
    figura(tamano)


# se llama a la función "tamaño_figura" dando inicio al programa
tamano_figura()
