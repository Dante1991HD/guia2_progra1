#!/usr/bin/env python3
# -*- coding:utf-8 -*-
matriz = []


# Función que imprime la matriz creada
def impresora(matriz):
    for i in range(len(matriz)):
        for j in range(len(matriz[i])):
            print(matriz[i][j], end='')
        print()
    print()


# Función que crea la matriz que contiene una de las figuras
def triangulos_inferior(altura):
    matriz3 = [['/', '-' * (altura * 2 - 1), '\\'] if i == 0 else [
        ' ' * i, '/', ' ' * ((altura * 2) - (2 * i + 1)), '\\', ' ' * i
    ] for i in range(altura)]
    matriz3.reverse()
    impresora(matriz3)


# Función que crea la matriz que contiene una de las figuras
def triangulos_superior(altura):
    matriz2 = [['\\', '-' * (altura * 2 - 1), '/'] if i == 0 else [
        ' ' * i, '\\', ' ' * ((altura * 2) - (2 * i + 1)), '/', ' ' * i
    ] for i in range(altura)]
    impresora(matriz2)


# Función que crea la matriz que contiene una de las figuras
def triangulos_laterales(altura):
    matriz = [[
        '|', ' ' * i, '\\', ' ' * ((altura * 2) - (2 * (i + 1))), '/',
        ' ' * (i), '|'
    ] if i < altura else [
        '|', ' ' * (altura * 2 - i - 1), '/', ' ' * (i * 2 - altura * 2), '\\',
        ' ' * (altura * 2 - i - 1), '|'
    ] for i in range(altura * 2)]
    impresora(matriz)


# Función que crea la matriz que contiene una de las figuras
def altura_triangulos():
    altura = int(input("Ingrese la altura de los triángulos "))
    triangulos_laterales(altura)
    triangulos_inferior(altura)
    triangulos_superior(altura)


# El llamado de esta función da inicio al programa
altura_triangulos()
