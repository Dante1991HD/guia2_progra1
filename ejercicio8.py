#!/usr/bin/python
# -*- coding: utf-8 -*-

# se importa la libreria random

import random

# Función que imprime la matriz
def impresora(matriz):
    for i in range(len(matriz)):
        for j in range(len(matriz[i])):
            print(matriz[i][j], end=' ')
        print()
    print()

# Función que intercambia los valores de la matriz
def invierte_matriz(matriz):
    matriz.reverse()
    impresora(matriz)

# Función que crea una matriz cuadrada con valores aleatorios
def crearmatriz():
    matriz = []
    for i in range(5):
        matriz.append([])
        for j in range(5):
            matriz[i].append(random.randint(0, 50))
    impresora(matriz)
    invierte_matriz(matriz)


# El llamando de esta función da inicio al programa
crearmatriz()
