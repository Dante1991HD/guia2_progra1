#!/usr/bin/python
# -*- coding: utf-8 -*-

# se importa la libreria random

import random
# se declaran las variables

lista = []
palabras = [
    'Buen', 'Día', 'a', 'Papá', 'Di', 'perro', 'gato', 'python', 'tomar'
]

# la función "listas_palabras_aleatorias" crea una lista de manera aleatoria con las palabras contenidas en otra lista


def listas_palabras_aleatorias():

    lista_1 = [random.choice(palabras) for x in range(9)]
    lista_2 = [random.choice(palabras) for x in range(9)]
    print("Lista Palabras aleatorias 1: ", lista_1,
          "\nLista Palabras aleatorias 1: ", lista_2)
    print("Intersección entre ambas", set(lista_1) & set(lista_2))
    print("Valores lista 1 menos la intersección", set(lista_1) - set(lista_2))
    print("Valores lista 2 menos la intersección", set(lista_2) - set(lista_1))


# El llamando de esta función da inicio al programa

listas_palabras_aleatorias()
