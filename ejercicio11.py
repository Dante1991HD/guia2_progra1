#!/usr/bin/python
# -*- coding: utf-8 -*-
#variable "entrada" de tipo lista
entrada = [5, 7, 2, 1, 3]
#se utiliza el algoritmo "quicksort" para el ordenamiento
def quicksort(entrada):

    if len(entrada) < 2:
        return entrada
    else:
        pivote = entrada[0]
        menor_que = [i for i in entrada[1:] if i < pivote + 1]
        mayor_que = [i for i in entrada[1:] if i > pivote]
        return quicksort(menor_que) + [pivote] + quicksort(mayor_que)

#se imprime el resultado que entrega la función con el algoritmo de ordenamiento
print(quicksort(entrada))