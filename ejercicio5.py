#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# Se importa la libreria random

import random


# Función que genera en base a 3 sublistas una independiente sin repetir los valores
def numeros():
    lista_a = [random.randint(0, 9) for x in range(3)]
    lista_b = [random.randint(0, 9) for x in range(3)]
    lista_c = [random.randint(0, 9) for x in range(3)]
    print(lista_a, lista_b, lista_c)
    lista_concatenada = lista_a + lista_b + lista_c
    print(lista_concatenada)
    lista_concatenada = list(dict.fromkeys(lista_concatenada))
    print(lista_concatenada)
    #si el valor es par lo intercambia por un aleatorio
    lista_concatenada = [i if i % 2 != 0 else random.randint(0, 9) for i in lista_concatenada]
    print(lista_concatenada)


# El llamado de esta función da inicio al programa
numeros()
