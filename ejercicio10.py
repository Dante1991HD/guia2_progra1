#!/usr/bin/python
# -*- coding: utf-8 -*-

#Función que decodifica un codigo
def decodificador():
    string = "¡XeXgXaXsXsXeXmX XtXeXrXcXeXsX XaX XsXiX XsXiXhXt"
    lista = list(string)
    mensaje_decifrado = [x for x in lista if x != 'X']
    mensaje_decifrado.reverse()
    mensaje_decifrado = ''.join(mensaje_decifrado)
    print(mensaje_decifrado)


# El llamando de esta función da inicio al programa
decodificador()
