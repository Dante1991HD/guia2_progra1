#!/usr/bin/python
# -*- coding: utf-8 -*-

# se importa la libreria random

import random

# se declaran las variables

lista = []
palabras = ['Buen', 'Día', 'a', 'Papá', 'Di']


# La función "invierte listas" recive la variable lista creada con palabra aleatorias y luego la invierte con .reverse

def invierte_listas(lista):
    lista.reverse()
    print(lista)


# la función "lista_palabras_aleatorias" crea una lista de manera aleatoria con las palabras contenidas en otra lista

def lista_palabras_aleatorias():
    for i in range(len(palabras)):
        lista.append(random.choice(palabras))
    print(lista)

    # la lista con palabras aleatorias creadas se pasa a la función que invierte_listas

    invierte_listas(lista)


# El llamando de esta función da inicio al programa

lista_palabras_aleatorias()
