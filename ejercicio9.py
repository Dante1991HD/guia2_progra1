#!/usr/bin/python
# -*- coding: utf-8 -*-


# Función que imprime la matriz
def impresora(matriz):
    for i in range(len(matriz)):
        for j in range(len(matriz[i])):
            print(matriz[i][j], end=' ')
        print()
    print()


# Función que rea una matriz identidad de 6x6
def crearmatriz():
    matriz = []
    for i in range(6):
        matriz.append([])
        for j in range(6):
            if i == j:
                matriz[i].append(1)
            else:
                matriz[i].append(0)
    impresora(matriz)


# El llamando de esta función da inicio al programa
crearmatriz()
